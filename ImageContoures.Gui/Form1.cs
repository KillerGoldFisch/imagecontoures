﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using GSharp.Logging;
using ImageContouresLib;
using ImageContouresLib.Filters;
using ImageContouresLib.Filters.Img;
using ImageContouresLib.Filters.Points;
using YamlConfig;

namespace ImageContoures.GUI
{
    public partial class Form1 : Form
    {
        private const string in_jpg = "In.jpg";
        private const string out_file = "contours.png";

        private string in_file;
        private int last_cam = -1;

        public Form1()
        {
            InitializeComponent();
            if (File.Exists(in_jpg))
                setImageViewIn(in_jpg);
            updateImageViewOut();

            string result;
            using (Stream stream = typeof(Form1).Assembly.GetManifestResourceStream("ImageContoures.Gui.default.yaml"))
            using (StreamReader reader = new StreamReader(stream))
            {
                result = reader.ReadToEnd();
            }
        }

        private void btn_cam_OnClick(object sender, EventArgs e)
        {
            setupCam();
        }

        private void btn_convert_OnClick(object sender, EventArgs e)
        {
            if (File.Exists(in_jpg))
                convert();
        }

        private void btn_file_picker_OnClick(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                setImageViewIn(openFileDialog1.FileName);
                File.Copy(in_file, in_jpg, true);
            }
        }

        private void setImageViewIn(string file_path)
        {
            //maybe someone renames test.txt to test.jpg just to be funny #breakthesystem
            try
            {
                var in_bmp = new Bitmap(file_path);
                img_viewer_in.Image = in_bmp;
                in_file = file_path;
            }
            catch (Exception)
            {
                // ignored
            }
        }

        private void updateImageViewOut()
        {
            //maybe someone renames test.txt to test.jpg just to be funny #breakthesystem
            try
            {
                var out_bmp = new Bitmap(out_file);
                img_viewer_out.Image = out_bmp;
            }
            catch (Exception)
            {
                // ignored
            }
        }

        private void convert()
        {
            var config = new Config(new FileInfo("config.yaml"));
            var env = config.GetMappingObject<ImageContouresEnv>("env");
            var omgYouKilledKenny = env.ImgFilter.OfType<Cenny>().FirstOrDefault();
            omgYouKilledKenny.Treshold = txt_threshold.IntValue;
            omgYouKilledKenny.Treshold_Linking = txt_threshold_linking.IntValue;
            Log.Success("Start converting");
            Application.DoEvents();
            env.StartImgFilter += env_StartImgFilter;
            env.StartConFilter += env_StartConFilter;
            env.Run();
            DrawContours f = env.ConFilter.OfType<DrawContours>().FirstOrDefault();
            try{
                img_viewer_out.Image = f.OutImg;
                Log.Success("Successfully converted image");
                tool_strip_label.Text = "Successful";
                saveBitmap(f.OutImg, out_file);
            }
            catch (ArgumentException){
                Log.Error("Error while converting");
                tool_strip_label.Text = "Error";
            }
            tool_strip_progress_bar.Value = 0;
        }

        private void env_StartConFilter(ImageContouresEnv arg1, IContoursFilter arg2)
        {
            arg2.ProgressChanged += arg2_ProgressChanged;
        }

        private void env_StartImgFilter(ImageContouresEnv arg1, IImgFilter arg2)
        {
            arg2.ProgressChanged += arg2_ProgressChanged;
        }

        private void arg2_ProgressChanged(IFilter obj)
        {
            tool_strip_progress_bar.Value = (int) (obj.Progress*100);
            tool_strip_label.Text = obj.ToString();
            Application.DoEvents();
        }

        private void setupCam()
        {
            CameraCapture.CameraCapture capture = new CameraCapture.CameraCapture();
            capture.Show();
            if (last_cam >= 0)
            {
                capture.SetupCapture(last_cam);
                capture.SelectedCamera = last_cam;
                capture.StartCapture();
            }
            while (capture.Image == null &&! capture.IsDisposed)
                Application.DoEvents();
            if (capture.Image != null)
            {
                if(img_viewer_in.Image != null)
                    img_viewer_in.Image.Dispose();
                img_viewer_in.Image = capture.Image;
                saveBitmap(capture.Image, in_jpg);
                last_cam = capture.SelectedCamera;
            }
            capture.Close();
        }

        /**
         * When either a Bitmap object or an Image object is constructed from a file, the file 
         * remains locked for the lifetime of the object. As a result, you cannot change an image 
         * and save it back to the same file where it originated. http://support.microsoft.com/?id=814675
         */
        private void saveBitmap(Bitmap bmp, string outputFileName)
        {
            using (MemoryStream memory = new MemoryStream())
            {
                using (FileStream fs = new FileStream(outputFileName, FileMode.Create, FileAccess.ReadWrite))
                {
                    bmp.Save(memory, ImageFormat.Png);
                    byte[] bytes = memory.ToArray();
                    fs.Write(bytes, 0, bytes.Length);
                }
            }
        }
    }
}