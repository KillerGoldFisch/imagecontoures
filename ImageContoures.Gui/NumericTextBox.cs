﻿using System.Globalization;
using System.Windows.Forms;

namespace ImageContoures.Gui
{
    /**
     * Pro Copy Paste Programmierer FTW
     * https://msdn.microsoft.com/de-de/library/ms229644(v=vs.80).aspx
     */

    public class NumericTextBox : TextBox
    {
        public NumericTextBox()
        {
            AllowSpace = false;
        }

        public int IntValue
        {
            get { return int.Parse(Text); }
        }

        public decimal DecimalValue
        {
            get { return decimal.Parse(Text); }
        }

        public bool AllowSpace { set; get; }

        // Restricts the entry of characters to digits (including hex), the negative sign,
        // the decimal point, and editing keystrokes (backspace).
        protected override void OnKeyPress(KeyPressEventArgs e)
        {
            base.OnKeyPress(e);

            var numberFormatInfo = CultureInfo.CurrentCulture.NumberFormat;
            var decimalSeparator = numberFormatInfo.NumberDecimalSeparator;
            var groupSeparator = numberFormatInfo.NumberGroupSeparator;
            var negativeSign = numberFormatInfo.NegativeSign;

            var keyInput = e.KeyChar.ToString();

            if (char.IsDigit(e.KeyChar))
            {
                // Digits are OK
            }
            //else if (keyInput.Equals(decimalSeparator) || keyInput.Equals(groupSeparator) ||
            // keyInput.Equals(negativeSign))
            //{
            //    // Decimal separator is OK
            //}
            else if (e.KeyChar == '\b')
            {
                // Backspace key is OK
            }
            //    else if ((ModifierKeys & (Keys.Control | Keys.Alt)) != 0)
            //    {
            //     // Let the edit control handle control and alt key combinations
            //    }
            else if (AllowSpace && e.KeyChar == ' ')
            {
            }
            else
            {
                // Swallow this invalid key and beep
                e.Handled = true;
                //    MessageBeep();
            }
        }
    }
}