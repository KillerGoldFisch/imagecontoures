﻿using ImageContoures.Gui;

namespace ImageContoures.GUI
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.split_container_vertical = new System.Windows.Forms.SplitContainer();
            this.split_container_horizontal = new System.Windows.Forms.SplitContainer();
            this.img_viewer_in = new KaiwaProjects.KpImageViewer();
            this.img_viewer_out = new KaiwaProjects.KpImageViewer();
            this.tool_strip = new System.Windows.Forms.StatusStrip();
            this.tool_strip_progress_bar = new System.Windows.Forms.ToolStripProgressBar();
            this.tool_strip_label = new System.Windows.Forms.ToolStripStatusLabel();
            this.log_view = new GSharp.UI.Controls.LogList();
            this.btn_choose_file = new System.Windows.Forms.Button();
            this.btn_sel_cam = new System.Windows.Forms.Button();
            this.btn_convert = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.label_threshold = new System.Windows.Forms.Label();
            this.label_threshold_linking = new System.Windows.Forms.Label();
            this.txt_threshold_linking = new ImageContoures.Gui.NumericTextBox();
            this.txt_threshold = new ImageContoures.Gui.NumericTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.split_container_vertical)).BeginInit();
            this.split_container_vertical.Panel1.SuspendLayout();
            this.split_container_vertical.Panel2.SuspendLayout();
            this.split_container_vertical.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.split_container_horizontal)).BeginInit();
            this.split_container_horizontal.Panel1.SuspendLayout();
            this.split_container_horizontal.Panel2.SuspendLayout();
            this.split_container_horizontal.SuspendLayout();
            this.tool_strip.SuspendLayout();
            this.SuspendLayout();
            // 
            // split_container_vertical
            // 
            this.split_container_vertical.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.split_container_vertical.Location = new System.Drawing.Point(1, 45);
            this.split_container_vertical.Name = "split_container_vertical";
            this.split_container_vertical.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // split_container_vertical.Panel1
            // 
            this.split_container_vertical.Panel1.Controls.Add(this.split_container_horizontal);
            // 
            // split_container_vertical.Panel2
            // 
            this.split_container_vertical.Panel2.Controls.Add(this.tool_strip);
            this.split_container_vertical.Panel2.Controls.Add(this.log_view);
            this.split_container_vertical.Size = new System.Drawing.Size(1360, 775);
            this.split_container_vertical.SplitterDistance = 454;
            this.split_container_vertical.TabIndex = 0;
            // 
            // split_container_horizontal
            // 
            this.split_container_horizontal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.split_container_horizontal.Location = new System.Drawing.Point(0, 0);
            this.split_container_horizontal.Name = "split_container_horizontal";
            // 
            // split_container_horizontal.Panel1
            // 
            this.split_container_horizontal.Panel1.Controls.Add(this.img_viewer_in);
            // 
            // split_container_horizontal.Panel2
            // 
            this.split_container_horizontal.Panel2.Controls.Add(this.img_viewer_out);
            this.split_container_horizontal.Size = new System.Drawing.Size(1360, 454);
            this.split_container_horizontal.SplitterDistance = 650;
            this.split_container_horizontal.TabIndex = 0;
            // 
            // img_viewer_in
            // 
            this.img_viewer_in.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            this.img_viewer_in.Dock = System.Windows.Forms.DockStyle.Fill;
            this.img_viewer_in.ForeColor = System.Drawing.SystemColors.ControlText;
            this.img_viewer_in.GifAnimation = false;
            this.img_viewer_in.GifFPS = 15D;
            this.img_viewer_in.Image = null;
            this.img_viewer_in.Location = new System.Drawing.Point(0, 0);
            this.img_viewer_in.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.img_viewer_in.MenuColor = System.Drawing.Color.LightSteelBlue;
            this.img_viewer_in.MenuPanelColor = System.Drawing.Color.LightSteelBlue;
            this.img_viewer_in.MinimumSize = new System.Drawing.Size(605, 193);
            this.img_viewer_in.Name = "img_viewer_in";
            this.img_viewer_in.NavigationPanelColor = System.Drawing.Color.LightSteelBlue;
            this.img_viewer_in.NavigationTextColor = System.Drawing.SystemColors.ButtonHighlight;
            this.img_viewer_in.OpenButton = false;
            this.img_viewer_in.PreviewButton = false;
            this.img_viewer_in.PreviewPanelColor = System.Drawing.Color.LightSteelBlue;
            this.img_viewer_in.PreviewText = "Preview";
            this.img_viewer_in.PreviewTextColor = System.Drawing.SystemColors.ButtonHighlight;
            this.img_viewer_in.Rotation = 0;
            this.img_viewer_in.Scrollbars = false;
            this.img_viewer_in.ShowPreview = true;
            this.img_viewer_in.Size = new System.Drawing.Size(650, 454);
            this.img_viewer_in.TabIndex = 0;
            this.img_viewer_in.TextColor = System.Drawing.SystemColors.ButtonHighlight;
            this.img_viewer_in.Zoom = 100D;
            // 
            // img_viewer_out
            // 
            this.img_viewer_out.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            this.img_viewer_out.Dock = System.Windows.Forms.DockStyle.Fill;
            this.img_viewer_out.ForeColor = System.Drawing.SystemColors.ControlText;
            this.img_viewer_out.GifAnimation = false;
            this.img_viewer_out.GifFPS = 15D;
            this.img_viewer_out.Image = null;
            this.img_viewer_out.Location = new System.Drawing.Point(0, 0);
            this.img_viewer_out.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.img_viewer_out.MenuColor = System.Drawing.Color.LightSteelBlue;
            this.img_viewer_out.MenuPanelColor = System.Drawing.Color.LightSteelBlue;
            this.img_viewer_out.MinimumSize = new System.Drawing.Size(605, 193);
            this.img_viewer_out.Name = "img_viewer_out";
            this.img_viewer_out.NavigationPanelColor = System.Drawing.Color.LightSteelBlue;
            this.img_viewer_out.NavigationTextColor = System.Drawing.SystemColors.ButtonHighlight;
            this.img_viewer_out.OpenButton = false;
            this.img_viewer_out.PreviewButton = false;
            this.img_viewer_out.PreviewPanelColor = System.Drawing.Color.LightSteelBlue;
            this.img_viewer_out.PreviewText = "Preview";
            this.img_viewer_out.PreviewTextColor = System.Drawing.SystemColors.ButtonHighlight;
            this.img_viewer_out.Rotation = 0;
            this.img_viewer_out.Scrollbars = false;
            this.img_viewer_out.ShowPreview = true;
            this.img_viewer_out.Size = new System.Drawing.Size(706, 454);
            this.img_viewer_out.TabIndex = 0;
            this.img_viewer_out.TextColor = System.Drawing.SystemColors.ButtonHighlight;
            this.img_viewer_out.Zoom = 100D;
            // 
            // tool_strip
            // 
            this.tool_strip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tool_strip_progress_bar,
            this.tool_strip_label});
            this.tool_strip.Location = new System.Drawing.Point(0, 292);
            this.tool_strip.Name = "tool_strip";
            this.tool_strip.Size = new System.Drawing.Size(1360, 25);
            this.tool_strip.TabIndex = 1;
            this.tool_strip.Text = "statusStrip1";
            // 
            // tool_strip_progress_bar
            // 
            this.tool_strip_progress_bar.Name = "tool_strip_progress_bar";
            this.tool_strip_progress_bar.Size = new System.Drawing.Size(100, 19);
            // 
            // tool_strip_label
            // 
            this.tool_strip_label.Name = "tool_strip_label";
            this.tool_strip_label.Size = new System.Drawing.Size(0, 20);
            // 
            // log_view
            // 
            this.log_view.Dock = System.Windows.Forms.DockStyle.Fill;
            this.log_view.Location = new System.Drawing.Point(0, 0);
            this.log_view.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.log_view.Name = "log_view";
            this.log_view.Size = new System.Drawing.Size(1360, 317);
            this.log_view.TabIndex = 0;
            // 
            // btn_choose_file
            // 
            this.btn_choose_file.AutoSize = true;
            this.btn_choose_file.Location = new System.Drawing.Point(12, 12);
            this.btn_choose_file.Name = "btn_choose_file";
            this.btn_choose_file.Size = new System.Drawing.Size(120, 27);
            this.btn_choose_file.TabIndex = 1;
            this.btn_choose_file.Text = "Bild auswaehlen";
            this.btn_choose_file.UseVisualStyleBackColor = true;
            this.btn_choose_file.Click += new System.EventHandler(this.btn_file_picker_OnClick);
            // 
            // btn_sel_cam
            // 
            this.btn_sel_cam.AutoSize = true;
            this.btn_sel_cam.Location = new System.Drawing.Point(138, 12);
            this.btn_sel_cam.Name = "btn_sel_cam";
            this.btn_sel_cam.Size = new System.Drawing.Size(75, 27);
            this.btn_sel_cam.TabIndex = 2;
            this.btn_sel_cam.Text = "Webcam";
            this.btn_sel_cam.UseVisualStyleBackColor = true;
            this.btn_sel_cam.Click += new System.EventHandler(this.btn_cam_OnClick);
            // 
            // btn_convert
            // 
            this.btn_convert.AutoSize = true;
            this.btn_convert.Location = new System.Drawing.Point(219, 12);
            this.btn_convert.Name = "btn_convert";
            this.btn_convert.Size = new System.Drawing.Size(91, 27);
            this.btn_convert.TabIndex = 3;
            this.btn_convert.Text = "Umwandeln";
            this.btn_convert.UseVisualStyleBackColor = true;
            this.btn_convert.Click += new System.EventHandler(this.btn_convert_OnClick);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // label_threshold
            // 
            this.label_threshold.AutoSize = true;
            this.label_threshold.Location = new System.Drawing.Point(316, 17);
            this.label_threshold.Name = "label_threshold";
            this.label_threshold.Size = new System.Drawing.Size(67, 17);
            this.label_threshold.TabIndex = 4;
            this.label_threshold.Text = "threshold";
            // 
            // label_threshold_linking
            // 
            this.label_threshold_linking.AutoSize = true;
            this.label_threshold_linking.Location = new System.Drawing.Point(495, 17);
            this.label_threshold_linking.Name = "label_threshold_linking";
            this.label_threshold_linking.Size = new System.Drawing.Size(115, 17);
            this.label_threshold_linking.TabIndex = 6;
            this.label_threshold_linking.Text = "threshold_linking";
            // 
            // txt_threshold_linking
            // 
            this.txt_threshold_linking.AllowSpace = false;
            this.txt_threshold_linking.Location = new System.Drawing.Point(616, 14);
            this.txt_threshold_linking.Name = "txt_threshold_linking";
            this.txt_threshold_linking.Size = new System.Drawing.Size(100, 22);
            this.txt_threshold_linking.TabIndex = 9;
            this.txt_threshold_linking.Text = "120";
            // 
            // txt_threshold
            // 
            this.txt_threshold.AllowSpace = false;
            this.txt_threshold.Location = new System.Drawing.Point(389, 14);
            this.txt_threshold.Name = "txt_threshold";
            this.txt_threshold.Size = new System.Drawing.Size(100, 22);
            this.txt_threshold.TabIndex = 8;
            this.txt_threshold.Text = "180";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1361, 819);
            this.Controls.Add(this.txt_threshold_linking);
            this.Controls.Add(this.txt_threshold);
            this.Controls.Add(this.label_threshold_linking);
            this.Controls.Add(this.label_threshold);
            this.Controls.Add(this.btn_convert);
            this.Controls.Add(this.split_container_vertical);
            this.Controls.Add(this.btn_sel_cam);
            this.Controls.Add(this.btn_choose_file);
            this.Name = "Form1";
            this.Text = "Form1";
            this.split_container_vertical.Panel1.ResumeLayout(false);
            this.split_container_vertical.Panel2.ResumeLayout(false);
            this.split_container_vertical.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.split_container_vertical)).EndInit();
            this.split_container_vertical.ResumeLayout(false);
            this.split_container_horizontal.Panel1.ResumeLayout(false);
            this.split_container_horizontal.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.split_container_horizontal)).EndInit();
            this.split_container_horizontal.ResumeLayout(false);
            this.tool_strip.ResumeLayout(false);
            this.tool_strip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SplitContainer split_container_vertical;
        private System.Windows.Forms.SplitContainer split_container_horizontal;
        private GSharp.UI.Controls.LogList log_view;
        private KaiwaProjects.KpImageViewer img_viewer_out;
        private KaiwaProjects.KpImageViewer img_viewer_in;
        private System.Windows.Forms.Button btn_convert;
        private System.Windows.Forms.Button btn_sel_cam;
        private System.Windows.Forms.Button btn_choose_file;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label label_threshold;
        private System.Windows.Forms.Label label_threshold_linking;
        private NumericTextBox txt_threshold;
        private NumericTextBox txt_threshold_linking;
        private System.Windows.Forms.StatusStrip tool_strip;
        private System.Windows.Forms.ToolStripProgressBar tool_strip_progress_bar;
        private System.Windows.Forms.ToolStripStatusLabel tool_strip_label;

    }
}

