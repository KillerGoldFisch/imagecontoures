using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using YamlConfig;


//Author : KEG
//Datum  : 28.07.2014 16:16:49
//Datei  : NearPoints.cs


namespace ImageContouresLib.Filters.Points {
    public class NearPoints : AContoursFilter {

        #region Members
        protected double _distance = 0.0;
        #endregion

        #region Events
        #endregion

        #region Initialization
        public NearPoints() {

        }
        #endregion

        #region Finalization
        ~NearPoints() {

        }
        #endregion

        #region Interface
        #endregion

        #region Interface (IPointFilter)
        public override void Run(ImageContouresEnv env) {
            this.Progress = 0.0;

            if (this.m_input == null)
                throw new Exception(string.Format("[{0}] Input-Contours is null"));

            List<Contour> cons = new List<Contour>();

            for (int i = 0; i < this.m_input.Contour.Length; i++) {
                Contour src = this.m_input.Contour[i];
                List<Vector2D> dst = new List<Vector2D>();

                Vector2D lastPoint = new Vector2D(999999.0, 999999.0);

                foreach (Vector2D p in src) {
                    if (p.Distance(lastPoint) < this._distance)
                        continue;
                    lastPoint = p.Clone();
                    dst.Add(lastPoint);
                }

                cons.Add(new Contour(dst.ToArray()));
                this.Progress = i / ((double)this.m_input.Contour.Length);
            }

            this.m_output = new Contours(cons.ToArray());

            this.Progress = 1.0;
        }
        #endregion

        #region Tools
        #endregion

        #region Browsable Properties
        #endregion

        #region NonBrowsable Properties
        #endregion
    }
}