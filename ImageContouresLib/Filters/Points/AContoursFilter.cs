using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using YamlConfig;


//Author : KEG
//Datum  : 29.07.2014 11:48:14
//Datei  : AContoursFilter.cs


namespace ImageContouresLib.Filters.Points {
    public abstract class AContoursFilter : MappingObject, IContoursFilter {

        #region Members
        protected Contours m_input = null;
        protected Contours m_output = null;
        private double m_progress = 0.0;
        #endregion

        #region Events
        #endregion

        #region Initialization
        public AContoursFilter() {

        }
        #endregion

        #region Finalization
        ~AContoursFilter() {

        }
        #endregion

        #region Interface
        public override string ToString() {
            return this.ConfigURL + " - " + this.GetType().Name;
        }
        #endregion

        #region Interface (IFilter)
        object IFilter.Output {
            get { return this.m_output; }
        }

        object IFilter.Input {
            get {
                return this.m_input;
            }
            set {
                this.m_input = value as Contours;
            }
        }

        public double Progress {
            get { return this.m_progress; }
            protected set {
                if (this.m_progress == value)
                    return;
                this.m_progress = value;
                this.RaiseProgressChanged();
            }
        }

        public abstract void Run(ImageContouresEnv env);

        public virtual void Reset() {
            this.Progress = 0.0;
            this.m_output = null;
        }

        public Controls.Filter.FilterControlBase GetControl() {
            throw new NotImplementedException();
        }
        #endregion

        #region Interface (IFilter<Contours>)
        public event Action<IFilter> ProgressChanged;

        public Contours Input {
            get {
                return this.m_input;
            }
            set {
                this.m_input = value;
            }
        }

        public Contours Output {
            get { return this.m_output; }
        }
        #endregion

        #region Tools
        protected void RaiseProgressChanged() {
            if (this.ProgressChanged != null)
                this.ProgressChanged(this);
        }
        #endregion

        #region Browsable Properties
        #endregion

        #region NonBrowsable Properties
        #endregion
    }
}