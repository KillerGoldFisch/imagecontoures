using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using YamlConfig;
using Microsoft.Scripting.Hosting;
using IronPython.Hosting;
using System.ComponentModel;
using System.IO;


//Author : KEG
//Datum  : 28.07.2014 18:13:00
//Datei  : CodeGen.cs


namespace ImageContouresLib.Filters.Points {
    public class PyScript : AContoursFilter {

        #region Members
        protected string _script = "";

        protected string[] _args = new String[] {"img.pgm"};
        #endregion

        #region Events
        #endregion

        #region Initialization
        public PyScript() {

        }
        #endregion

        #region Finalization
        ~PyScript() {

        }
        #endregion

        #region Interface
        #endregion

        #region Interface (IPointFilter)
        public override void Run(ImageContouresEnv env) {
            this.Progress = 0.0;

            if (this.m_input == null)
                throw new Exception(string.Format("[{0}] Input-Contours is null", this.ConfigURL));

            ScriptEngine pyEngine = null;
            ScriptScope pyScope = null;

            pyEngine = Python.CreateEngine();
            pyScope = pyEngine.CreateScope();

            var paths = pyEngine.GetSearchPaths();
            paths.Add("scripts/Lib");
            paths.Add(Path.GetFullPath("scripts/Lib.zip"));
            pyEngine.SetSearchPaths(paths);

            pyScope.SetVariable("env", env);
            pyScope.SetVariable("inputcon", this.m_input);
            pyScope.SetVariable("ifilter", this);
            pyScope.SetVariable("args", this._args);

            ScriptSource src = pyEngine.CreateScriptSourceFromFile(this._script);

            src.Compile();
            src.Execute(pyScope);
            
            this.m_output = pyScope.GetVariable<Contours>("outputcon");


            if(this.m_output == null)
                this.m_output = this.m_input;
            this.Progress = 1.0;
        }
        #endregion

        #region Tools
        #endregion

        #region Browsable Properties
        [Editor(typeof(ImageContouresLib.Controls.Helper.TypeEditors.PyScriptFileSelectorTypeEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public String ScriptFile {
            get { return this._script; }
            set { this._script = value; }
        }
        #endregion

        #region NonBrowsable Properties
        #endregion
    }
}