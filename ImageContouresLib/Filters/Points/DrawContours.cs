using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using YamlConfig;
using System.Drawing;


//Author : KEG
//Datum  : 28.07.2014 18:49:14
//Datei  : DrawContours.cs


namespace ImageContouresLib.Filters.Points {
    public class DrawContours : AContoursFilter {

        #region Members
        protected string _file = @"contours.png";

        protected double _scale = 1.0;

        protected bool _drawconnections = false;

        protected bool _save = false;

        private Bitmap img = null;
        #endregion

        #region Events
        #endregion

        #region Initialization
        public DrawContours() {

        }
        #endregion

        #region Finalization
        ~DrawContours() {

        }
        #endregion

        #region Interface
        #endregion

        #region Interface (IPointFilter)
        public override void Run(ImageContouresEnv env) {
            this.Progress = 0.0;

            if (this.m_input == null)
                throw new Exception(string.Format("[{0}] Input-Contours is null"));

            RectangleF bounding = this.m_input.GetBounding();
            Vector2D offset = new Vector2D(-bounding.Location.X, -bounding.Location.Y);

            Bitmap bmp = new Bitmap((int)(bounding.Width * this._scale), (int)(bounding.Height * this._scale));
            Graphics g = Graphics.FromImage(bmp);

            Vector2D last = null;
            Vector2D prev = null;

            Pen p1 = new Pen(Color.Blue, 1.0f);
            Pen p2 = new Pen(Color.Red, 3.0f);

            //g.Clear(Color.White);

            for (int i = 0; i < this.m_input.Contour.Length; i++ ) {
                var c = this.m_input.Contour[i];
                prev = null;
                if (last != null && this._drawconnections)
                    g.DrawLine(p1, (Point)((last + offset) * this._scale), (Point)((c.First + offset) * this._scale));
                foreach (var v in c) {
                    if (prev != null)
                        g.DrawLine(p2, (Point)((prev + offset) * this._scale), (Point)((v + offset) * this._scale));
                    prev = v;
                }
                last = c.Last;

                this.Progress = i / (double)this.m_input.Contour.Length;
            }

            //g.Dispose();

            this.img = bmp;

            if(this._save)
                bmp.Save(this._file);

            this.m_output = this.m_input;
            this.Progress = 1.0;
        }
        #endregion

        #region Tools
        #endregion

        #region Browsable Properties

        public Bitmap OutImg
        {
            get
            {
                return this.img;
            }
        }
        #endregion

        #region NonBrowsable Properties
        #endregion
    }
}