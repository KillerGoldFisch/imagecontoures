using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using YamlConfig;


//Author : KEG
//Datum  : 28.07.2014 16:42:29
//Datei  : SmallContours.cs


namespace ImageContouresLib.Filters.Points {
    public class SmallContours : AContoursFilter {

        #region Members
        protected double _minlen = 0.0;
        #endregion

        #region Events
        #endregion

        #region Initialization
        public SmallContours() {

        }
        #endregion

        #region Finalization
        ~SmallContours() {

        }
        #endregion

        #region Interface
        #endregion

        #region Interface (IPointFilter)
        public override void Run(ImageContouresEnv env) {
            this.Progress = 0.0;

            if (this.m_input == null)
                throw new Exception(string.Format("[{0}] Input-Contours is null"));

            List<Contour> cons = new List<Contour>();

            for (int i = 0; i < this.m_input.Contour.Length; i++) {
                Contour src = this.m_input.Contour[i];

                if (src.CLength > this._minlen)
                    cons.Add(src);

                this.Progress = i / ((double)this.m_input.Contour.Length);
            }

            this.m_output = new Contours(cons.ToArray());

            this.Progress = 1.0;
        }
        #endregion

        #region Tools
        #endregion

        #region Browsable Properties
        #endregion

        #region NonBrowsable Properties
        #endregion
    }
}