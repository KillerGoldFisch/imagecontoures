using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using YamlConfig;


//Author : KEG
//Datum  : 28.07.2014 16:47:58
//Datei  : PathOptimation.cs


namespace ImageContouresLib.Filters.Points {
    public class PathOptimation : AContoursFilter {

        #region Members
        protected double _minlen = 0.0;
        #endregion

        #region Events
        #endregion

        #region Initialization
        public PathOptimation() {

        }
        #endregion

        #region Finalization
        ~PathOptimation() {

        }
        #endregion

        #region Interface
        #endregion

        #region Interface (IPointFilter)
        public override void Run(ImageContouresEnv env) {
            this.Progress = 0.0;

            if (this.m_input == null)
                throw new Exception(string.Format("[{0}] Input-Contours is null"));

            List<Contour> consSrc = new List<Contour>(this.m_input.Contour);
            List<Contour> consDst = new List<Contour>();

            Vector2D lastPoint = new Vector2D();

            while(consSrc.Count > 0) {
                Contour next = consSrc[0];
                double nextDist = double.MaxValue;
                double currentDist = 0.0;

                foreach (Contour con in consSrc) {
                    currentDist = con.First.Distance(lastPoint);
                    if (currentDist < nextDist) {
                        nextDist = currentDist;
                        next = con;
                    }
                }

                consDst.Add(next);
                consSrc.Remove(next);

                lastPoint = next.Last;

                this.Progress = consDst.Count / ((double)this.m_input.Contour.Length);
            }

            this.m_output = new Contours(consDst.ToArray());

            this.Progress = 1.0;
        }
        #endregion

        #region Tools
        #endregion

        #region Browsable Properties
        #endregion

        #region NonBrowsable Properties
        #endregion
    }
}