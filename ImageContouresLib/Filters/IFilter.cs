﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ImageContouresLib.Controls.Filter;

namespace ImageContouresLib.Filters {
    public interface IFilter<T> : IFilter {
        T Input { get; set; }
        T Output { get; }
    }

    public interface IFilter : IConfigObject {

        event Action<IFilter> ProgressChanged;

        Object Input { get; set; }
        Object Output { get; }

        double Progress { get; }

        void Run(ImageContouresEnv env);

        void Reset();

        FilterControlBase GetControl();
    }
}
