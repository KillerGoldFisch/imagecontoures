using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using YamlConfig;
using System.Drawing;
using Emgu.CV.Structure;
using Emgu.CV;
using GSharp.Logging;
using System.ComponentModel;


//Author : KEG
//Datum  : 28.07.2014 16:02:38
//Datei  : FileSourcecs.cs


namespace ImageContouresLib.Filters.Img {
    public class FileSourcecs : AImgFilter {

        public enum ScaleMode {
            Scale,
            Fit
        }

        #region Members
        protected string _file = @"input.bmp";
        protected Vector2D _offset = new Vector2D();
        protected double _scale = 1.0;
        protected double _rotation = 0.0;
        protected Vector2D _fit = null;
        protected ScaleMode _scalemode = ScaleMode.Scale;
        protected ScaleMode _sizemode = ScaleMode.Scale;
        protected Vector2D _size = null;
        #endregion

        #region Events
        #endregion

        #region Initialization
        public FileSourcecs() {

        }
        #endregion

        #region Finalization
        ~FileSourcecs() {

        }
        #endregion

        #region Interface
        #endregion

        #region Interface (IImgFilter)
        public override void Run(ImageContouresEnv env) {
            this.Progress = 0.0;

            Bitmap bmpIn = new Bitmap(this._file);

            if (this._size != null)
                bmpIn = resize(bmpIn, this._size, this._sizemode);

            Image<Gray, byte> img = new Image<Gray, byte>(bmpIn);

            if (this._scalemode == ScaleMode.Fit) {
                if (null != this._fit) { 
                    this._scale = this._fit.X / bmpIn.Width;
                    this._scale = Math.Min(this._fit.Y / bmpIn.Height, this._scale);
                }else{
                    Log.Warn(string.Format("[{0}] ScaleMode==Fit, but not fit Value!"));
                }
            }

            this.m_output = new PImage(img, this._offset, this._scale, this._rotation);

            env.Dict["size"] = new Vector2D(img.Width * _scale, img.Height * _scale);

            this.Progress = 1.0;
        }
        #endregion

        #region Tools

        private Bitmap resize(Bitmap toResize, Vector2D size, ScaleMode mode)
        {
            switch (mode)
            {
                    case ScaleMode.Scale:
                        return new Bitmap(toResize, new Size((int)(size.X), (int)(size.Y)));
                    break;
                    case ScaleMode.Fit:
                        float scale = Math.Min((float)size.X / toResize.Width, (float)size.Y / toResize.Height);
                        return new Bitmap(toResize, new Size((int)(toResize.Width*scale), (int)(toResize.Height*scale)));
                    break;
            }
            return toResize;
        }
        #endregion

        #region Browsable Properties
        [Editor(typeof(ImageContouresLib.Controls.Helper.TypeEditors.ImageSelectorTypeEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public String File { get { return this._file; } set { this._file = value; } }
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Vector2D Offset { get { return this._offset; } set { this._offset = value; } }
        public double Scale { get { return this._scale; } set { this._scale = value; } }
        public double Rotation { get { return this._rotation; } set { this._rotation = value; } }
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Vector2D Fit { get { return this._fit; } set { this._fit = value; } }
        public ScaleMode Scalemode { get { return this._scalemode; } set { this._scalemode = value; } }
        #endregion

        #region NonBrowsable Properties
        #endregion
    }
}