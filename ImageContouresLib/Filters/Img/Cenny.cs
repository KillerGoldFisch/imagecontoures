using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emgu.CV;
using Emgu.CV.Structure;
using ImageContouresLib.Controls.Helper;


//Author : KEG
//Datum  : 28.07.2014 15:24:07
//Datei  : Cenny.cs


namespace ImageContouresLib.Filters.Img {
    [Client("Cenny", "Ein Cenny-Edge Filter", "ImageContouresLib.Icons.filter-icon.png")]
    public class Cenny : AImgFilter {

        #region Members
        protected double _treshold = 180.0;
        protected double _treshold_linking = 120.0;
        #endregion

        #region Events
        #endregion

        #region Initialization
        public Cenny() {

        }
        #endregion

        #region Finalization
        ~Cenny() {

        }
        #endregion

        #region Interface
        #endregion

        #region Interface (IImgFilter)
        public override void Run(ImageContouresEnv env) {
            this.Progress = 0.0;
            if (this.m_input == null)
                throw new Exception(string.Format("[{0}] Input Image is null!", this.ConfigURL));

            Image<Gray, Byte> canny = this.m_input.Image.Canny(this._treshold, this._treshold_linking);
            this.m_output = new PImage(canny, this.m_input.Offset, this.m_input.Scale, this.m_input.Rotation);
            this.Progress = 1.0;
        }
        #endregion

        #region Tools
        #endregion

        #region Browsable Properties
        public double Treshold {
            get { return _treshold; }
            set {
                if (_treshold == value)
                    return;
                _treshold = value;
            }
        }

        public double Treshold_Linking {
            get { return _treshold_linking; }
            set {
                if (_treshold_linking == value)
                    return;
                _treshold_linking = value;
            }
        }
        #endregion

        #region NonBrowsable Properties
        #endregion

    }
}