﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ImageContouresLib.Filters.Img {
    public interface IImgFilter: IFilter<PImage> {
        PImage Output { get; set; }
    }
}
