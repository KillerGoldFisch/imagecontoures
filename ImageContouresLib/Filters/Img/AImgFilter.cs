using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using YamlConfig;
using ImageContouresLib.Controls.Filter;


//Author : KEG
//Datum  : 29.07.2014 11:41:51
//Datei  : AImgFilter.cs


namespace ImageContouresLib.Filters.Img {
    public abstract class AImgFilter : MappingObject, IImgFilter {

        #region Members
        protected PImage m_input = null;
        protected PImage m_output = null;

        protected double m_progress = 0.0;
        #endregion

        #region Events
        #endregion

        #region Initialization
        public AImgFilter() {

        }
        #endregion

        #region Finalization
        ~AImgFilter() {

        }
        #endregion

        #region Interface
        public override string ToString() {
            return this.ConfigURL + " - " + this.GetType().Name;
        }
        #endregion

        #region Interface (IFilter)
        public event Action<IFilter> ProgressChanged;

        object IFilter.Input {
            get {
                return this.m_input;
            }
            set {
                this.m_input = value as PImage;
            }
        }

        object IFilter.Output {
            get { return this.m_output; }
        }

        PImage IImgFilter.Output
        {
            get { return m_output; }
            set { m_output = value; }
        }

        public double Progress {
            get { return this.m_progress; }
            protected set {
                if (value == this.m_progress)
                    return;
                this.m_progress = value;
                this.RaiseProgressChanged();
            }
        }

        public abstract void Run(ImageContouresEnv env);

        public virtual void Reset() {
            this.Progress = 0.0;
            this.m_output = null;
        }

        public Controls.Filter.FilterControlBase GetControl() {
            return new ImgFilterControl() { Filter = this };
        }
        #endregion

        #region Interface (IFilter<PImage>)
        public PImage Input {
            get {
                return this.m_input;
            }
            set {
                this.m_input = value;
            }
        }

        public PImage Output {
            get { return this.m_output; }
            set {
                this.m_output = value;
            }
        }
        #endregion

        #region Tools
        protected void RaiseProgressChanged() {
            if (this.ProgressChanged != null)
                this.ProgressChanged(this);
        }
        #endregion

        #region Browsable Properties
        #endregion

        #region NonBrowsable Properties
        #endregion

    }
}