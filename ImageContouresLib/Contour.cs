using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;


//Author : KEG
//Datum  : 16.06.2014 09:08:19
//Datei  : Contour.cs


namespace ImageContouresLib {
    public class Contour : IEnumerable<Vector2D> {

        #region Members
        private Vector2D[] data;
        private float len = 0.0f;
        #endregion

        #region Events
        #endregion

        #region Initialization
        public Contour(Vector2D[] data) {
            this.data = data;
            this._calcLen();
        }

        public Contour(IEnumerable<PointF> data) {
            List<Vector2D> vdata = new List<Vector2D>();

            foreach (PointF p in data)
                vdata.Add(new Vector2D(p));

            this.data = vdata.ToArray();

            this._calcLen();
        }
        #endregion

        #region Finalization
        ~Contour() {

        }
        #endregion

        #region Interface
        #endregion

        #region Tools
        private void _calcLen() {
            len = 0.0f;
            for (int i = 1; i < data.Length; i++)
                len += (float)(data[i - 1].Distance(data[i]));
        }
        #endregion

        #region Browsable Properties
        public Vector2D this[int index] {
            get {
                if( index >= 0)
                    return this.data[index];
                return this.data[this.data.Length + index];
            }
        }

        public int Count {
            get { return this.data.Length; }
        }

        public float CLength {
            get { return this.len; }
        }

        public Vector2D First { get { return this.data[0]; } }
        public Vector2D Last { get { return this.data[this.data.Length - 1]; } }
        #endregion

        #region NonBrowsable Properties
        #endregion

        public IEnumerator<Vector2D> GetEnumerator() {
            foreach (Vector2D v in this.data)
                yield return v;
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator() {
            return this.data.GetEnumerator();
        }
    }
}