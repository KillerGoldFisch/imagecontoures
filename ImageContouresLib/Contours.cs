using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emgu.CV;
using System.Drawing;


//Author : KEG
//Datum  : 28.07.2014 16:19:44
//Datei  : Contours.cs


namespace ImageContouresLib {
    public class Contours : IEnumerable<Contour> {

        #region Members
        protected Contour[] m_contours;

        protected RectangleF m_bounding = RectangleF.Empty;
        #endregion

        #region Events
        #endregion

        #region Initialization
        public Contours(Contour[] contours) {
            this.m_contours = contours;
        }

        public Contours(PImage img) {
            List<Contour> cont = new List<Contour>();

            using (MemStorage storage = new MemStorage()) //allocate storage for contour approximation
                for (
                   Contour<Point> contours = img.Image.FindContours(
                      Emgu.CV.CvEnum.CHAIN_APPROX_METHOD.CV_CHAIN_APPROX_SIMPLE,
                      Emgu.CV.CvEnum.RETR_TYPE.CV_RETR_LIST,
                      storage);
                   contours != null;
                   contours = contours.HNext) {
                    PointF[] pointsTmp = new PointF[contours.Count()];
                    List<Vector2D> points = new List<Vector2D>();
                    for (int j = 0; j < contours.Count(); j++) {
                        Point p = contours[j];
                        points.Add((new Vector2D(p.X, p.Y) + img.Offset).Rotate(img.Rotation)*img.Scale);
                    }

                    cont.Add(new Contour(points.ToArray()));
                }

            this.m_contours = cont.ToArray();

        }
        #endregion

        #region Finalization
        ~Contours() {

        }
        #endregion

        #region Interface
        public Contour this[int index] {
            get {
                return this.m_contours[index];
            }
        }

        public int GetTotalCount() {
            int i = 0;
            foreach (var c in this)
                i += c.Count;
            return i;
        }

        public RectangleF GetBounding() {
            if (this.m_bounding != RectangleF.Empty)
                return this.m_bounding;

            double x_min = double.MaxValue;
            double x_max = double.MinValue;
            double y_min = double.MaxValue;
            double y_max = double.MinValue;

            foreach (var con in this.Contour) {
                foreach (var v in con) {
                    x_min = Math.Min(x_min, v.X);
                    x_max = Math.Max(x_max, v.X);
                    y_min = Math.Min(y_min, v.Y);
                    y_max = Math.Max(y_max, v.Y);
                }
            }

            this.m_bounding = new RectangleF((float)x_min, (float)y_min, (float)(x_max - x_min), (float)(y_max - y_min));
            return this.m_bounding;
        }
        #endregion

        #region Tools
        #endregion

        #region Browsable Properties
        public int Length {
            get {
                return this.m_contours.Length;
            }
        }

        public Contour[] Contour {
            get { return this.m_contours; }
        }

        public Vector2D Min {
            get {
                RectangleF bounding = this.GetBounding();
                return new Vector2D(bounding.X, bounding.Y);
            }
        }

        public Vector2D Max {
            get {
                RectangleF bounding = this.GetBounding();
                return new Vector2D(bounding.X + bounding.Width, bounding.Y + bounding.Height);
            }
        }
        #endregion

        #region NonBrowsable Properties
        #endregion

        public IEnumerator<Contour> GetEnumerator() {
            foreach (var c in this.m_contours)
                yield return c;
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator() {
            foreach (var c in this.m_contours)
                yield return c;
        }
    }
}