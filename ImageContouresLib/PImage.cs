using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using Emgu.CV;
using Emgu.CV.Structure;


//Author : KEG
//Datum  : 28.07.2014 15:25:07
//Datei  : Img.cs


namespace ImageContouresLib {
    public class PImage {

        #region Members
        protected Vector2D _offset;
        protected double _scale;
        protected double _rotation;

        protected Image<Gray, Byte> _img;
        #endregion

        #region Events
        #endregion

        #region Initialization
        public PImage(Image<Gray, Byte> img, Vector2D offset = null, double scale = 1.0, double rotation = 0.0) {
            this._img = img;
            if (offset == null)
                this._offset = new Vector2D();
            else
                this._offset = offset;
            this._rotation = rotation;
            this._scale = scale;
        }

        public PImage(PImage other) {
            this._img = other._img.Clone();
            this._offset = other._offset.Clone();
            this._rotation = other._rotation;
            this._scale = other._scale;
        }
        #endregion

        #region Finalization
        ~PImage() {

        }
        #endregion

        #region Interface
        #endregion

        #region Tools
        #endregion

        #region Browsable Properties
        public Image<Gray, Byte> Image { get { return this._img; } }
        public Vector2D Offset { get { return this._offset; } }
        public double Scale { get { return this._scale; } }
        public double Rotation { get { return this._rotation; } }
        #endregion

        #region NonBrowsable Properties
        #endregion
    }
}