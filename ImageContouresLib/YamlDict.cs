using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using YamlConfig;
using QiHe.Yaml.Grammar;


//Author : KEG
//Datum  : 29.07.2014 15:14:18
//Datei  : YamlDict.cs


namespace ImageContouresLib {
    public class YamlDict : Dictionary<string, object>,  IMappingObject {

        #region Members
        protected MappingWrapper m_mapping;
        #endregion

        #region Events
        #endregion

        #region Initialization
        public YamlDict() {

        }
        #endregion

        #region Finalization
        ~YamlDict() {

        }
        #endregion

        #region Interface
        #endregion

        #region Interface (IMappingObject)
        public string ConfigURL {
            get {
                if (this.m_mapping == null)
                    return "?!?";
                return this.m_mapping.GetURL();
            }
        }

        public MappingWrapper Mapping {
            get { return this.m_mapping; }
        }

        public void OnShutDown(Config conf) {
            
        }

        public void ReadMapping(MappingWrapper mapping, Config config) {
            this.m_mapping = mapping;

            foreach (DataItem key in mapping.Keys) {
                string skey = key.GetTextOrDefault(null);
                if (skey == null)
                    continue;
                object val = mapping[key].ToType(mapping.Config);
                this[skey] = val;
            }
        }
        #endregion

        #region Tools
        #endregion

        #region Browsable Properties
        #endregion

        #region NonBrowsable Properties
        #endregion
    }
}