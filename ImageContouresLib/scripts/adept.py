import sys

#sys.path.insert(0, r'C:\prog\ImageContouresLib\ImageContoures.Console\bin\Debug\scripts\Lib.zip')  # Add .zip file to front of path

import template
import traceback

try:

	x_min = 999999
	x_max = -999999
	y_min = 999999
	y_max = -999999

	for c in inputcon:
		for p in c:
			x_min = min(x_min, p.X)
			x_max = max(x_max, p.X)
			y_min = min(y_min, p.Y)
			y_max = max(y_max, p.Y)


	pgm = template.render(
		filename=r'scripts/adept.templ', 
		context={
			'env':env,
			'contours': inputcon,
			'ifilter': ifilter,
			'args': args,
			'x_min': x_min,
			'x_max': x_max,
			'y_min': y_min,
			'y_max': y_max
		})

	pgm = pgm.replace("\r\n", "\n")
	pgm = pgm.replace("\n\n", "\n")

	f = open('img.v2', "w")
	f.write(pgm)
	f.close()

except Exception, ex:
	 print(traceback.format_exc())

global outputcon
outputcon = None
