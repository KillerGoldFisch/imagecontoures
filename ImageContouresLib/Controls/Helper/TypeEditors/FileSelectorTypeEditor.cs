using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing.Design;
using System.ComponentModel;
using System.Windows.Forms.Design;
using System.Windows.Forms;
using System.IO;


//Author : KEG
//Datum  : 13.02.2015 09:40:46
//Datei  : FileSelectorTypeEditor.cs


namespace ImageContouresLib.Controls.Helper.TypeEditors {
    /// <summary>
    /// PropertyGrid Attribute for FileDialog support.
    /// Usage: [Editor(typeof(FileSelectorTypeEditor), typeof(UITypeEditor))]
    /// </summary>
    public class FileSelectorTypeEditor : UITypeEditor {

        #region Members
        protected string m_filter = "All Files (*.*)|*.*";
        #endregion

        #region Events
        #endregion

        #region Initialization
        public FileSelectorTypeEditor() {

        }

        public FileSelectorTypeEditor(string filter) {
            this.m_filter = filter;
        }
        #endregion

        #region Finalization
        ~FileSelectorTypeEditor() {

        }
        #endregion

        #region Interface
        #endregion

        #region Inteface (UITypeEditor)
        public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context) {
            if (context == null || context.Instance == null)
                return base.GetEditStyle(context);
            return UITypeEditorEditStyle.Modal;
        }

        public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value) {
            IWindowsFormsEditorService editorService;

            if (context == null || context.Instance == null || provider == null)
                return value;

            try {
                // get the editor service, just like in windows forms
                editorService = (IWindowsFormsEditorService)
                   provider.GetService(typeof(IWindowsFormsEditorService));

                OpenFileDialog dlg = new OpenFileDialog();
                dlg.Filter = this.m_filter != null ? this.m_filter : "All Files (*.*)|*.*";
                dlg.CheckFileExists = true;

                string filename = (string)value;
                if (!File.Exists(filename))
                    filename = null;
                dlg.FileName = filename;

                using (dlg) {
                    DialogResult res = dlg.ShowDialog();
                    if (res == DialogResult.OK) {
                        filename = dlg.FileName;
                    }
                }
                return filename;

            } finally {
                editorService = null;
            }
        }
        #endregion

        #region Tools
        #endregion

        #region Browsable Properties
        #endregion

        #region NonBrowsable Properties
        #endregion
    }
}