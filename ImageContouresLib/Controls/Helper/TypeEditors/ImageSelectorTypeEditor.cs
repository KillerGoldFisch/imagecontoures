﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ImageContouresLib.Controls.Helper.TypeEditors {
    public class ImageSelectorTypeEditor : FileSelectorTypeEditor {
        public ImageSelectorTypeEditor() : base(
            "JPEG Files (*.jpeg)|*.jpeg|PNG Files (*.png)|*.png|JPG Files (*.jpg)|*.jpg|GIF Files (*.gif)|*.gif"
            ) {}
    }
}
