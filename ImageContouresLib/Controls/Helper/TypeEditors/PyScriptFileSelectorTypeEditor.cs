﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ImageContouresLib.Controls.Helper.TypeEditors {
    public class PyScriptFileSelectorTypeEditor : FileSelectorTypeEditor {
        public PyScriptFileSelectorTypeEditor() : base("Python Script Files (*.py)|*.py") { }
    }
}
