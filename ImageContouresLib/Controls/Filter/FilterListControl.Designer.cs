﻿namespace ImageContouresLib.Controls.Filter {
    partial class FilterListControl {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.flpEnv = new System.Windows.Forms.FlowLayoutPanel();
            this.flpAvFilter = new System.Windows.Forms.FlowLayoutPanel();
            this.ttFilter = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.flpEnv);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.flpAvFilter);
            this.splitContainer1.Size = new System.Drawing.Size(1761, 755);
            this.splitContainer1.SplitterDistance = 1460;
            this.splitContainer1.TabIndex = 0;
            // 
            // flpEnv
            // 
            this.flpEnv.AllowDrop = true;
            this.flpEnv.AutoScroll = true;
            this.flpEnv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpEnv.Location = new System.Drawing.Point(0, 0);
            this.flpEnv.Name = "flpEnv";
            this.flpEnv.Size = new System.Drawing.Size(1460, 755);
            this.flpEnv.TabIndex = 0;
            // 
            // flpAvFilter
            // 
            this.flpAvFilter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpAvFilter.Location = new System.Drawing.Point(0, 0);
            this.flpAvFilter.Name = "flpAvFilter";
            this.flpAvFilter.Size = new System.Drawing.Size(297, 755);
            this.flpAvFilter.TabIndex = 0;
            // 
            // FilterListControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer1);
            this.Name = "FilterListControl";
            this.Size = new System.Drawing.Size(1761, 755);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.FlowLayoutPanel flpEnv;
        private System.Windows.Forms.FlowLayoutPanel flpAvFilter;
        private System.Windows.Forms.ToolTip ttFilter;

    }
}
