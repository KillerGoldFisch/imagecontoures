﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ImageContouresLib.Filters;

namespace ImageContouresLib.Controls.Filter{
    public partial class FilterControlBase : UserControl {

        protected IFilter m_filter = null;
        protected ImageContouresEnv m_env = null;

        public Action<FilterControlBase> RequestUp;
        public Action<FilterControlBase> RequestDown;
        public Action<FilterControlBase> RequestRemove;

        public FilterControlBase() {
            InitializeComponent();

            this.pgFilter.PropertyValueChanged +=new PropertyValueChangedEventHandler(pgFilter_PropertyValueChanged);
        }

        public IFilter Filter {
            get { return this.m_filter; }
            set {
                if (this.m_filter == value)
                    return;
                IFilter old = this.m_filter;
                this.m_filter = value;
                this.OnNewFilter(old);
                this.UpdateFilter();
            }
        }

        public ImageContouresEnv Env {
            get { return this.m_env; }
            set {
                if (this.m_env == value)
                    return;
                this.m_env = value;
                this.UpdateFilter();
            }
        }

        protected virtual void OnUpdatePreview() { }
        protected virtual void OnNewFilter(IFilter old) {
            if (old != null) {
                old.ProgressChanged -= new Action<IFilter>(m_filter_ProgressChanged);
            }
            this.pbFilter.Value = 0;
            this.m_filter.ProgressChanged += new Action<IFilter>(m_filter_ProgressChanged);
        }

        private void m_filter_ProgressChanged(IFilter obj) {
            this.pbFilter.Value = (int)(obj.Progress * this.pbFilter.Maximum);
        }

        public void UpdateFilter() {
            this.pgFilter.SelectedObject = this.m_filter;
            if (this.m_filter != null && this.m_env != null) {
                this.m_filter.Run(this.m_env);
                this.pbFilter.Value = this.pbFilter.Maximum;
            }
            this.OnUpdatePreview();
        }

        private void pgFilter_PropertyValueChanged(object s, PropertyValueChangedEventArgs e) {
            this.UpdateFilter();
        }

        private void btnUp_Click(object sender, EventArgs e) {
            if (this.RequestUp != null)
                this.RequestUp(this);
        }

        private void btnDown_Click(object sender, EventArgs e) {
            if (this.RequestDown != null)
                this.RequestDown(this);
        }

        private void btnRemove_Click(object sender, EventArgs e) {
            if (this.RequestRemove != null)
                this.RequestRemove(this);
        }
    }
}
