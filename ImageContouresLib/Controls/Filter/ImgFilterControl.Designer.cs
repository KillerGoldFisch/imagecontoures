﻿namespace ImageContouresLib.Controls.Filter {
    partial class ImgFilterControl {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent() {
            this.ivPreview = new KaiwaProjects.KpImageViewer();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.ivPreview);
            // 
            // ivPreview
            // 
            this.ivPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ivPreview.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            this.ivPreview.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ivPreview.GifAnimation = false;
            this.ivPreview.GifFPS = 15D;
            this.ivPreview.Image = null;
            this.ivPreview.Location = new System.Drawing.Point(6, 25);
            this.ivPreview.MenuColor = System.Drawing.Color.LightSteelBlue;
            this.ivPreview.MenuPanelColor = System.Drawing.Color.LightSteelBlue;
            this.ivPreview.MinimumSize = new System.Drawing.Size(454, 157);
            this.ivPreview.Name = "ivPreview";
            this.ivPreview.NavigationPanelColor = System.Drawing.Color.LightSteelBlue;
            this.ivPreview.NavigationTextColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ivPreview.OpenButton = false;
            this.ivPreview.PreviewButton = false;
            this.ivPreview.PreviewPanelColor = System.Drawing.Color.LightSteelBlue;
            this.ivPreview.PreviewText = "Preview";
            this.ivPreview.PreviewTextColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ivPreview.Rotation = 0;
            this.ivPreview.Scrollbars = false;
            this.ivPreview.ShowPreview = true;
            this.ivPreview.Size = new System.Drawing.Size(838, 429);
            this.ivPreview.TabIndex = 1;
            this.ivPreview.TextColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ivPreview.Zoom = 100D;
            // 
            // ImgFilterControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "ImgFilterControl";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private KaiwaProjects.KpImageViewer ivPreview;
    }
}
