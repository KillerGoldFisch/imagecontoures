﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ImageContouresLib.Filters.Img;

namespace ImageContouresLib.Controls.Filter {
    public partial class ImgFilterControl : FilterControlBase {
        public ImgFilterControl() {
            InitializeComponent();
        }

        protected override void OnUpdatePreview() {
            base.OnUpdatePreview();

            if (this.ImgFilter == null)
                return;
            if (this.ImgFilter.Output == null)
                return;
            this.ivPreview.Image = this.ImgFilter.Output.Image.ToBitmap();
        }

        protected override void OnNewFilter(Filters.IFilter old) {
            base.OnNewFilter(old);
            this.ivPreview.Image = null;
        }

        public AImgFilter ImgFilter {
            get {
                return this.Filter as AImgFilter;
            }
            set {
                this.Filter = value;
            }
        }
    }
}
