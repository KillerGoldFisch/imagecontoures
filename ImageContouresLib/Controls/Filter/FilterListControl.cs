﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ImageContouresLib.Filters;
using GSharp.Logging;
using GSharp.Extensions.ObjectEx;
using GSharp.Extensions.ArrayEx;
using GSharp.Extensions.IEnumerableEx;
using GSharp.Extensions.BitmapEx;
using ImageContouresLib.Controls.Helper;

namespace ImageContouresLib.Controls.Filter {
    public partial class FilterListControl : UserControl {
        public const string DEFAULTICONPATH = "ImageContouresLib.Icons.filter-icon.png";

        private object m_initValue = null;

        private Type m_filtertype = typeof(IFilter);

        protected static Bitmap sm_default_icon = null;

        public FilterListControl() {
            if (sm_default_icon == null) {
                var stream = this.GetType().Assembly.GetManifestResourceStream(DEFAULTICONPATH);
                if (stream != null)
                    sm_default_icon = new Bitmap(stream).Resize(90,90);
            }

            InitializeComponent();
        }

        public void SetInitValue(object val) {
            this.m_initValue = val;
        }

        public void SetFilterType(Type filterType) {
            this.m_filtertype = filterType;
        }

        public void SetAviableFilter(Type[] filterTypes) {
            flpAvFilter.Controls.Clear();

            foreach (Type filterType in filterTypes) {
                if (!IsFilterCompatible(filterType)) {
                    Log.Warn(string.Format("Can't add Type '{0}' to Filter-List", filterType), filterType);
                    continue;
                }


                String name = filterType.Name;
                String dircr = "";
                Bitmap icon = sm_default_icon; // TODO: default icon.

                System.Attribute[] attrs = System.Attribute.GetCustomAttributes(filterType);  // Reflection. 

                // Displaying output. 
                foreach (System.Attribute attr in attrs) {
                    if (attr is ClientAttribute) {
                        ClientAttribute ca = (ClientAttribute)attr;
                        name = ca.Name;
                        dircr = ca.Description;
                        if(ca.Icon != null)
                            icon = ca.Icon;
                    }
                }

                Button btnFilterType = new Button();
                btnFilterType.ForeColor = Color.DarkBlue;
                // btnFilterType.Text = filterType.Name;
                this.ttFilter.SetToolTip(btnFilterType, name + "\n\n" + dircr);
                btnFilterType.Tag = filterType;
                btnFilterType.BackgroundImage = icon;
                btnFilterType.Height = 90;
                btnFilterType.Width = 90;
                btnFilterType.Click += new EventHandler(btnFilterType_Click);
                flpAvFilter.Controls.Add(btnFilterType);
            }
        }

        public bool IsFilterCompatible(Type filterType) {
            if (filterType == typeof(Object))
                return false;

            if (filterType != m_filtertype && !filterType.IsSubclassOf(m_filtertype) && m_filtertype.IsAssignableFrom(filterType)) {
                if (filterType.IsInterface)
                    return filterType.GetInterfaces().ForeachOneTrue(IsFilterCompatible);
                else
                    return IsFilterCompatible(filterType.BaseType) || filterType.GetInterfaces().ForeachOneTrue(IsFilterCompatible);
            } else return true;
        }

        void btnFilterType_Click(object sender, EventArgs e) {
            Type type = (sender as Control).Tag as Type;
            IFilter filter = Activator.CreateInstance(type) as IFilter;
            this.AddFilter(filter);
        }

        public void AddFilter(IFilter filter) {
            if(!filter.IsCastableTo(m_filtertype)) {
                throw new Exception(string.Format("Can't cast Type '{0}' to '{1}'", filter.GetType(), m_filtertype));
            }
            FilterControlBase fcontrol = filter.GetControl();
            if(this.Filter.Length > 0)
                fcontrol.Filter.Input = this.Filter.Get(-1);
            fcontrol.UpdateFilter();
            this.flpEnv.Controls.Add(fcontrol);

            fcontrol.RequestDown = Filter_RequestDown;
            fcontrol.RequestRemove = Filter_RequestRemove;
            fcontrol.RequestUp = Filter_RequestUp;
        }

        public bool RemoveFilter(IFilter f) {
            var doRemove = from c in this.FilterControls where c.Filter == f select c;
            bool removed = false;
            foreach (var c in doRemove) {
                this.flpEnv.Controls.Remove(c);
                removed = true;
            }
            return removed;
        }

        public IFilter[] Filter {
            get {
                return (from c in this.FilterControls select c.Filter).ToArray();
            }
        }

        public FilterControlBase[] FilterControls {
            get {
                return (from Control c in this.flpEnv.Controls select c as FilterControlBase).ToArray();
            }
        }

        protected virtual void Filter_RequestDown(FilterControlBase filter) {
            int index = this.flpEnv.Controls.GetChildIndex(filter);
            this.flpEnv.Controls.SetChildIndex(filter, index + 1);
        }

        protected virtual void Filter_RequestRemove(FilterControlBase filter) {
            this.flpEnv.Controls.Remove(filter);
            filter.Dispose();
        }

        protected virtual void Filter_RequestUp(FilterControlBase filter) {
            int index = this.flpEnv.Controls.GetChildIndex(filter);
            this.flpEnv.Controls.SetChildIndex(filter, index - 1);
        }
    }
}
