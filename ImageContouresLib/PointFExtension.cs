﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace ImageContouresLib
{
    public static class PointFExtension
    {
        public static PointF Add(this PointF this_, PointF other)
        {
            return new PointF(this_.X + other.X, this_.Y + other.Y);
        }

        public static PointF Add(this PointF this_, float other)
        {
            return new PointF(this_.X + other, this_.Y + other);
        }

        public static PointF Multiply(this PointF this_, float other)
        {
            return new PointF(this_.X * other, this_.Y * other);
        }
    }
}
