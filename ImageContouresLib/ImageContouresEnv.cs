using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using YamlConfig;
using GSharp.Extensions.ObjectEx;
using ImageContouresLib.Filters.Img;
using GSharp.Logging;
using ImageContouresLib.Filters.Points;
using ImageContouresLib.Filters;


//Author : KEG
//Datum  : 29.07.2014 10:55:47
//Datei  : ImageContouresEnv.cs


namespace ImageContouresLib {
    public class ImageContouresEnv : MappingObject {

        #region Members
        protected Array _imgfilter = new Object[] { };
        protected Array _confilter = new Object[] { };

        protected IFilter m_currentFilter = null;

        private Func<bool> m_runAction = null;

        private YamlDict _dict = new YamlDict();
        #endregion

        #region Events
        public event Action<ImageContouresEnv, IImgFilter> StartImgFilter;
        public event Action<ImageContouresEnv, IContoursFilter> StartConFilter;
        #endregion

        #region Initialization
        public ImageContouresEnv() {
            this.m_runAction = new Func<bool>(this.Run);
        }
        #endregion

        #region Finalization
        ~ImageContouresEnv() {

        }
        #endregion

        #region Interface
        public bool Run() {
            try {
                PImage img = null;

                foreach (var imf in this.ImgFilter) {
                    imf.Reset();
                    imf.Input = img;
                    this.m_currentFilter = imf;
                    this.RaiseStartImgFilter(imf);
                    imf.Run(this);
                    img = imf.Output;
                }

                Contours con = new Contours(img);

                foreach (var cof in this.ConFilter) {
                    cof.Reset();
                    cof.Input = con;
                    this.m_currentFilter = cof;
                    this.RaiseStartConFilter(cof);
                    cof.Run(this);
                    con = cof.Output;
                }

                //Cleanup bitmaps, so we don't keep any file locks
                foreach (var imf in this.ImgFilter) {
                    //God I hate nullchecking
                    if (imf.Input != null)
                    {
                        imf.Input.Image.Dispose();
                        imf.Input = null;
                    }
                    if (imf.Output != null)
                    {
                        imf.Output.Image.Dispose();
                        imf.Output = null;
                    }
                }

                return true;
            } catch (Exception ex) {
                Log.Exception(string.Format("[{0}] Exception while Running", this.ConfigURL), ex);
                return false;
            }
        }
        #endregion

        #region Interface (IMappingObject)
        protected override void ReadMapping(MappingWrapper mapping) {
            base.ReadMapping(mapping);

            List<Object> filter = new List<object>();

            lock (this._imgfilter) {
                for (int i = 0; i < this._imgfilter.Length; i++)
                    if (!this._imgfilter.GetValue(i).IsCastableTo(typeof(IImgFilter))) {
                        Log.Warn(string.Format("[{0}].imgfilter[{1}] is not a IImgFilter!", this.ConfigURL, i));

                    } else {
                        filter.Add(this._imgfilter.GetValue(i));
                    }

                this._imgfilter = filter.ToArray();
                filter.Clear();
            }

            lock (this._confilter) {
                for (int i = 0; i < this._confilter.Length; i++)
                    if (!this._confilter.GetValue(i).IsCastableTo(typeof(IContoursFilter))) {
                        Log.Warn(string.Format("[{0}].confilter[{1}] is not a IContoursFilter!", this.ConfigURL, i));

                    } else {
                        filter.Add(this._confilter.GetValue(i));
                    }

                this._confilter = filter.ToArray();
            }

        }
        #endregion

        #region Tools
        protected void RaiseStartImgFilter(IImgFilter filter) {
            if (this.StartImgFilter != null)
                this.StartImgFilter(this, filter);
        }

        protected void RaiseStartConFilter(IContoursFilter filter) {
            if (this.StartConFilter != null)
                this.StartConFilter(this, filter);
        }
        #endregion

        #region Browsable Properties
        public IDictionary<string, object> Dict {
            get {
                return this._dict;
            }
        }

        public Func<bool> RunAction { get { return this.m_runAction; } }

        public IImgFilter[] ImgFilter {
            get { return this._imgfilter.OfType<IImgFilter>().ToArray(); }
            set { 
                lock(this._imgfilter)
                    this._imgfilter = value; 
            }
        }

        public IContoursFilter[] ConFilter {
            get { return this._confilter.OfType<IContoursFilter>().ToArray(); }
            set {
                lock(this._confilter)
                    this._confilter = value; 
            }
        }
        #endregion

        #region NonBrowsable Properties
        #endregion
    }
}