﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

/*
|| 
|| MIRIA - http://miria.codeplex.com
|| Copyright (C) 2008-2011 Generoso Martello <generoso@martello.com>
||
|| This program is free software: you can redistribute it and/or modify
|| it under the terms of the GNU General Public License as published by
|| the Free Software Foundation, either version 3 of the License, or
|| (at your option) any later version.
||  
|| This program is distributed in the hope that it will be useful,
|| but WITHOUT ANY WARRANTY; without even the implied warranty of
|| MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
|| GNU General Public License for more details.
|| 
|| You should have received a copy of the GNU General Public License
|| along with this program. If not, see
|| <http://www.gnu.org/licenses/>.
|| 
*/

namespace ImageContouresLib {
  /// <summary>
  /// Summary description for Vector2D.
  /// </summary>
  public class Vector2D : YamlConfig.MappingObject
  {
    private double _x, _y;
    public static readonly Vector2D Empty = new Vector2D();

    public Vector2D() : this(0.0,0.0) { }

    public Vector2D(PointF p) : this(p.X, p.Y) { }

    public Vector2D(double x, double y)
    {
      this._x = x;
      this._y = y;
    }

    #region Properties
    public double X
    {
      get { return _x; }
      set { _x = value; }
    }

    public double Y
    {
      get { return _y; }
      set { _y = value; }
    }
    #endregion

    #region Object
    public Vector2D Clone() {
        return new Vector2D(this._x, this._y);
    }

    public override bool Equals(object obj)
    {
      if (obj is Vector2D)
      {
        Vector2D v = (Vector2D)obj;
        if (v._x == _x && v._y == _y)
          return obj.GetType().Equals(this.GetType());
      }
      return false;
    }

    public override int GetHashCode()
    {
      return base.GetHashCode ();
    }

    public override string ToString()
    {
      return String.Format("{{X={0}, Y={1}}}", _x, _y);
    }
    #endregion

    public double Len()
    {
      return Math.Sqrt(_x*_x + _y*_y);
    }

    public double Distance(Vector2D v) {
        return Vector2D.Distance(this, v);
    }

    public Vector2D Rotate(double theta) {

        double x = this._x * Math.Cos(theta) + this._y * -Math.Sin(theta);
        double y = this._x * Math.Sin(theta) + this._y * Math.Cos(theta);

        return new Vector2D(x, y);
    }

    /*public static bool operator==(Vector2D u, Vector2D v)
    {
      if (u.X == v._x && u._y == v._y)
        return true;
      else
        return false;
    }*/

    /*public static bool operator !=(Vector2D u, Vector2D v)
    {
      return u != v;
    }*/

    public static Vector2D operator+(Vector2D u, Vector2D v)
    {
      return new Vector2D(u._x + v._x, u._y + v._y);
    }

    public static Vector2D operator-(Vector2D u, Vector2D v)
    {
      return new Vector2D(u._x - v._x, u._y - v._y);
    }

    public static Vector2D operator*(Vector2D u, double a)
    {
      return new Vector2D(a*u._x, a*u._y);
    }

    public static Vector2D operator/(Vector2D u, double a)
    {
      return new Vector2D(u._x/a, u._y/a);
    }

    public static Vector2D operator-(Vector2D u)
    {
      return new Vector2D(-u._x, -u._y);
    }


        public static double GetAngle(PointF c)
        {
            // angolo retta passante per l'origine
            return GetAngle(c, new Point(0, 0));
        }
        public static double GetAngle(PointF c, PointF f)
        {
            // angolo in gradi tra i due punti c ed f - gradi
//            return Math.Atan((f.X - c.X) / (f.Y - c.Y) / Math.PI * 180 );
            return (180 * (1 + Math.Atan2((c.Y - f.Y), (c.X - f.X)) / Math.PI));
        }
        public static double Distance(PointF a, PointF b)
        {
            return Math.Sqrt(Math.Pow((a.X - b.X),2) + Math.Pow((a.Y - b.Y), 2));
        }

        public static double Distance(Vector2D a, Vector2D b) {
            return Math.Sqrt(Math.Pow((a.X - b.X), 2) + Math.Pow((a.Y - b.Y), 2));
        }

        public static PointF Rotate(PointF point, PointF origin, double angle)
        {
            double X = origin.X + ((point.X - origin.X) * Math.Cos(angle) -
                (point.Y - origin.Y) * Math.Sin(angle));
            double Y = origin.Y + ((point.X - origin.X) * Math.Sin(angle) +
                (point.Y - origin.Y) * Math.Cos(angle));
            return new PointF((float)X, (float)Y);
        }
        public static PointF GetPoint(Point origin, double length, double angle)
        {
            return new PointF((float)(origin.X + length * Math.Cos(angle)),
                (float)(origin.Y + length * Math.Sin(angle)));
        }

    public static explicit operator PointF(Vector2D u)
    {
      return new PointF((float)u._x, (float)u._y);
    }

    public static explicit operator Point(Vector2D u) {
        return new Point((int)u._x, (int)u._y);
    }

    public static implicit operator Vector2D(Point p)
    {
      return new Vector2D(p.X, p.Y);
    }

  }
}

   
    
    