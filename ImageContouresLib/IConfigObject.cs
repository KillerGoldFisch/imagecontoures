﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ImageContouresLib {
    public interface IConfigObject {
        string ToYaml(int indent);
    }
}
