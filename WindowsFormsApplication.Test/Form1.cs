﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ImageContouresLib.Filters;
using ImageContouresLib;

namespace WindowsFormsApplication.Test {
    public partial class Form1 : Form {
        public Form1() {
            InitializeComponent();

            //this.filterListControl1.SetFilterType(typeof(IFilter<PImage>));

            this.filterListControl1.SetAviableFilter(new Type[]{
                typeof(ImageContouresLib.Filters.Img.FileSourcecs),
                typeof(ImageContouresLib.Filters.Img.Cenny),
            });
        }
    }
}
