﻿namespace WindowsFormsApplication.Test {
    partial class Form1 {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent() {
            this.filterListControl1 = new ImageContouresLib.Controls.Filter.FilterListControl();
            this.SuspendLayout();
            // 
            // filterListControl1
            // 
            this.filterListControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.filterListControl1.Location = new System.Drawing.Point(0, 0);
            this.filterListControl1.Name = "filterListControl1";
            this.filterListControl1.Size = new System.Drawing.Size(1245, 795);
            this.filterListControl1.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1245, 795);
            this.Controls.Add(this.filterListControl1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private ImageContouresLib.Controls.Filter.FilterListControl filterListControl1;
    }
}

