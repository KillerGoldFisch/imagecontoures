﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using YamlConfig;
using System.IO;
using QiHe.Yaml.Grammar;
using ImageContouresLib.Filters.Img;
using ImageContouresLib.Filters.Points;
using ImageContouresLib;
using GSharp.Extensions.StringEx;

namespace ImageContoures.Console {
    class Program {
        static void Main(string[] args) {
            GSharp.Logging.Log.LoggingHandler.Add(new GSharp.Logging.Logger.ConsoleLogger());

            Config conf;
            string result;
            using (Stream stream = typeof(Program).Assembly.GetManifestResourceStream("ImageContoures.Console.default.yaml"))
            using (StreamReader reader = new StreamReader(stream)) {
                result = reader.ReadToEnd();
            }

            List<IImgFilter> imgFilter = new List<IImgFilter>();
            List<IContoursFilter> conFilter = new List<IContoursFilter>();

            if(args.Length>1)
                conf = new Config(new FileInfo(args[0]), result);
            else
                conf = new Config(new FileInfo("config.yaml"), result);

            ImageContouresEnv env = conf.GetMappingObject<ImageContouresEnv>("env");

            //IContoursFilter f = env.ConFilter[2];

            env.StartImgFilter += new Action<ImageContouresEnv, IImgFilter>(env_StartImgFilter);
            env.StartConFilter += new Action<ImageContouresEnv, IContoursFilter>(env_StartConFilter);

            env.Run();

            System.Console.ReadKey();
        }

        static void PrintProgress(double progress) {
            int size = 40;
            //System.Console.Write("\r");
            System.Console.SetCursorPosition(0, System.Console.CursorTop);
            System.Console.Write("[");
            int iprogress = (int)(progress * size);
            int iprogress_ = size - iprogress;
            System.Console.Write("#".RepeatToLen(iprogress));
            System.Console.Write(" ".RepeatToLen(iprogress_));
            System.Console.Write("] ");
        }

        static void env_StartConFilter(ImageContouresEnv arg1, IContoursFilter arg2) {
            System.Console.WriteLine();
            arg2.ProgressChanged += new Action<ImageContouresLib.Filters.IFilter>(arg2_ProgressChanged);
        }

        static void env_StartImgFilter(ImageContouresEnv arg1, IImgFilter arg2) {
            System.Console.WriteLine();
            arg2.ProgressChanged += new Action<ImageContouresLib.Filters.IFilter>(arg2_ProgressChanged);
        }

        static void arg2_ProgressChanged(ImageContouresLib.Filters.IFilter obj) {
            PrintProgress(obj.Progress);
            System.Console.Write(obj.ToString().Truncate(36));
        }
    }
}
